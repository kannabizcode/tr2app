Ext.define('TR2.Application', {
	extend: 'Ext.app.Application',
	name: 'TR2',
	requires: ['TR2.*'],

	views: [
        'TR2.view.login.TR2LoginView',
        'TR2.view.main.TR2MainView',
    ],


	launch: function () {
		let loggedIn;
        loggedIn = localStorage.getItem("LoggedIn");

        Ext.widget(loggedIn ? 'TR2main' : 'TR2login');
	}
});
