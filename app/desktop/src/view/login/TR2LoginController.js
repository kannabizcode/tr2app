Ext.define('TR2.view.login.TR2LoginController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.login',

    mixins: [
        'Ext.mixin.Keyboard'
    ],

    // Login
    clickLogin: function() {
        localStorage.setItem("LoggedIn", true);
        this.getView().destroy();
        Ext.widget('TR2main');
    },
    enterLogin: function(f,e){  
        if (e.getKey() == e.ENTER) {
         localStorage.setItem("LoggedIn", true);
         this.getView().destroy();
         Ext.widget('TR2main');
        }
     } 
});