Ext.define('TR2.view.login.TR2LoginView', {
    extend: 'Ext.container.Viewport',
    xtype: 'TR2login',
    controller: 'login',
    title: 'Tripod login',
    id: 'tripodLogin',
    layout: {
        type: 'hbox',
        align: 'middle',
        pack: 'center'
    },
    items: [{
        xtype: 'panel',
        title: 'Tripod Login',
        bodyPadding: 10,
        width: 400,
        items: [{
            xtype: 'form',
            reference: 'form',
            items: [{
                xtype: 'textfield',
                name: 'username',
                fieldLabel: 'Username',
                allowBlank: false,
                width: '100%'
            }, {
                xtype: 'textfield',
                name: 'password',
                inputType: 'password',
                fieldLabel: 'Password',
                allowBlank: false,
                width: '100%',
                listeners: {
                    specialkey: 'enterLogin'
                },
            }, {
                xtype: 'displayfield',
                hideEmptyLabel: false,
                value: '<strong>username: </strong> demo <br> <strong>password:</strong> demo'
            }],
            buttons: [{
                text: 'Login',
                formBind: true,
                listeners: {
                    click: 'clickLogin'
                }
            }]
        }]

    }]

});