Ext.define('TR2.view.explorer.TR2Explorer', {
    extend: 'Ext.Panel',
    xtype: 'TR2Explorer',
    items: [
        {
            xtype: 'panel',
            height: 400,
            width: '100%',
        },
        {
            xtype: 'panel',
            title: 'Actions',
            height: '100%',
            layout: 'fit',
            height: '100%',
            width: '100%',
            split: true
        },
    ]
  })