Ext.define('TR2.view.main.TR2MainView', {
  extend: 'Ext.container.Viewport',
  xtype: 'TR2main',
  controller: 'mainController',
  layout: 'border',
  items: [
  {
    region: 'north',
    title: 'north',
    xtype: 'TR2Toolbar'
  },
  {
    region: 'center',    
    xtype: 'TR2Container',
    layout: 'vbox'
},
  {
    title: 'Footer',
    region: 'south',   
    xtype: 'panel'
},{
    title: 'Explorer',
    region:'west',
    xtype: 'TR2Explorer',
    width: 200,
    collapsible: true,  
    id: 'west-region-container',
    layout: 'vbox',
    split: true
}
],
})