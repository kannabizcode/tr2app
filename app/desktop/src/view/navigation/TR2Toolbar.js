Ext.define('TR2.view.navigation.TR2Toolbar', {
  extend: 'Ext.Toolbar',
  xtype: 'TR2Toolbar',
  items: [{
      text: 'File',
      menu: {
        items: [{
          iconCls: 'x-fa fa-save',
          text: 'Save'
        }]
      }
    },
    {
      text: 'Edit',
      menu: {
        items: [{
          iconCls: 'x-fa fa-plus',
          text: 'New case'
        }]
      }
    }
  ]
})